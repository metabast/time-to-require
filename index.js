module.exports = function(moduleName) {
	saveTime = Date.now() / 1000;
	module = require(moduleName);
	time = String(Date.now() / 1000 - saveTime);
	split = time.split('.');
	if(split.length>1)
		time = split[0]+'.'+split[1].substr(0,3);
	console.log( 'Module "' + moduleName + '" loaded on ' + time + ' sec' );
	return module;
}